import { NavLink } from 'react-router-dom';
import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
import home_icon from './assets/icons/home_icon.png';
import work_icon from './assets/icons/work_icon.png';
import resume_icon from './assets/icons/resume_icon.png';



function NavbarDarkExample() {
    return (
        <Navbar className="navbar navbar-expand-lg bg-dark sticky-top py-0 class=" data-bs-theme="dark" variant="dark" bg="dark" expand="lg">
            <Container fluid>
                <Navbar.Brand href="/"></Navbar.Brand>
                <Navbar.Toggle aria-controls="navbar-dark-example" />
                <Navbar.Collapse id="navbar-dark-example">
                    <Nav>
                        <ul className="navbar-nav me-auto mb-2">
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/">
                                     About</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/projects/">
                                     Projects</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/resume/">
                                    Resume</NavLink>
                            </li>
                        </ul>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}


export default NavbarDarkExample;