import React from 'react';
import Typewriter from 'typewriter-effect'
import Skills from "./Components/Skills";
import { Container } from 'react-bootstrap'

class TaylorInfo extends React.Component {
    render() {
        return (

            <div className="App">
                <div class="gradient"></div>
                <div
                    className="container-fluid d-flex align-items-center"
                    style={{
                        height: "100%",
                        width: '100%',
                        backgroundImage:
                            "url(https://images.pexels.com/photos/1276518/pexels-photo-1276518.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1)",
                        backgroundPosition: "center",
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        opacity: 1.0,
                    }}
                >
                    <Container fluid>
                        <Container fluid>
                            <div>
                                <figure className="figure">
                                    <img src="https://puppy-love-assets.s3.us-west-1.amazonaws.com/media/Taylor+-+headshot.JPG" className='img-pp' alt="avatar" height='275px' width='275px' />
                                    <figcaption className="figure-caption text-center"></figcaption>
                                    <figcaption className='under_profpic'>
                                        <ul></ul>

                                        <th>
                                            <ul className='img-pp'>
                                                <a href="https://gitlab.com/taylorpanningmade" target='_blank' rel='noreferrer' className='icon-colour  home-social-icons'>
                                                    <img src="https://puppy-love-assets.s3.us-west-1.amazonaws.com/media/4373151_gitlab_logo_logos_icon.png" alt="gitlab" />
                                                </a>
                                            </ul>
                                        </th>
                                        <th>
                                            <ul className='img-pp'>
                                                <a href="https://www.linkedin.com/in/taylorpanning/" target='_blank' rel='noreferrer' className='icon-colour  home-social-icons'>
                                                    <img src="https://puppy-love-assets.s3.us-west-1.amazonaws.com/media/317725_linkedin_social_icon+(1).png" alt="gitlab" />
                                                </a>
                                            </ul>
                                        </th>
                                        <th>
                                            <ul className='img-pp'>
                                                <a href="mailto:tpanning6@gmail.com" target='_blank' rel='noreferrer' className='icon-colour  home-social-icons'>
                                                    <img src="https://puppy-love-assets.s3.us-west-1.amazonaws.com/media/4202011_email_gmail_mail_logo_social_icon.png" alt="gitlab" />
                                                </a>
                                            </ul>
                                        </th>

                                    </figcaption>
                                </figure>
                            </div>
                        </Container>
                    <ul></ul>
                    <div className="card text-bg-dark d-grid gap-3"
                        style={{
                            opacity: .8,
                        }}
                    >
                        <div className='row'>
                            <h3>
                                <div className='typewriter1'>
                                    <Typewriter
                                        onInit={(typewriter) => {
                                            typewriter
                                                .typeString("< Taylor_Ross_Panning />")
                                                .pauseFor(4000)
                                                .deleteAll()
                                                .typeString("< Software_Engineer />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Explorer />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Musician />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Chef />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Green_Thumb />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Gamer Q('.'Q) />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Professional />")
                                                .pauseFor(1000)
                                                .deleteAll()
                                                .typeString("< Taylor_Ross_Panning />")
                                                .start();
                                        }}
                                    />
                                </div>
                            </h3>
                        </div>
                        <Container>
                        <div className="fs-6 fw-light lh-sm p-2 bg-dark border w-30 p-3">
                            I am a multifaceted software engineer with the ability to not only meet development needs but human needs.
                            Growing up, I would take apart every old computer, toy, or gadget to understand how and why they worked.
                            I am fascinated with the unknown and I am always looking to teach myself something new every day.
                            My background in psychology, neuroscience, and sales all required my acute attention to detail, my ability to learn new skills quickly and my ability to empathize.
                            Now I want to use my past experiences to create projects that will make a positive impact on people, organizations, and society at large.
                        </div>
                        <hr style={{border: '1px solid gray', width: '100%'}} />
                        <div className="fs-6 fw-light lh-sm p-2 bg-dark border w-30 p-3">
                            At The Ohio State University, I studied Neuroscience because of my passion for being on the cutting edge of discovery.
                            While in school I worked on the suicide hotline for Columbus, OH, and researched a potential new medication for schizophrenia.
                            As I distilled my interests in later years I realized that utilizing new technologies to improve the world around me is what excites me most, so I studied to become a software engineer.
                            I received an advanced software engineering certificate from Hack Reactor 19-week immersive program and have been coding ever since.
                        </div>
                        <hr style={{border: '1px solid gray', width: '100%'}} />
                        <div className="fs-6 fw-light lh-sm p-2 bg-dark border w-30 p-3">
                            If I am not camping with friends or skiing on the weekend, you can likely find me trail running, cooking a new recipe, at a farmers market, learning a new song on the guitar, or playing a game.
                            I love to stay up to date on the latest consumer technologies, philosophize over political issues, or discuss beer brewing recipes.
                        </div>
                        <div>
                            Interested in chatting about a job opportunity on your team?
                        </div>
                        <ul>
                            Send me an email at tpanning6@email.com
                        </ul>
                        </Container>

                    </div>
                <div class="gradient"></div>
                </Container>
                </div>
                <div>
                    <ul></ul>
                    <ul></ul>
                    <ul></ul>
                    <Skills></Skills>
                </div>
            </div >

        );
    }
}

export default TaylorInfo

