import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { sendToVercelAnalytics } from './vitals';
import Nav from './Nav';
import TaylorInfo from './main';
import Resume from './resume';
import NewProjects from './NewProjects';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router />
  </React.StrictMode>
);

function Router() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route exact path="/" element={<TaylorInfo />} />
        <Route path="/resume/" element={<Resume />} />
        <Route path='/projects/' element={<NewProjects />} />
      </Routes>
    </BrowserRouter>
  );
};

reportWebVitals(sendToVercelAnalytics);
