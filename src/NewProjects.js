import React, { useState } from "react";
import todovid from './assets/videos/todovid.gif';
import puppy2 from './assets/videos/puppy2.gif';
import Carcar from './assets/videos/Carcar.gif';
import carcar from './assets/icons/carcar.jpg'
import scrumpt1 from './assets/videos/scrump1.gif'

import './App.css'


const NewProjects = () =>  {
    const [isHovered, setIsHovered] = useState(false);
    
    const handleMouseOver = () => {
        setIsHovered(true);
    };
    
    const handleMouseOut = () => {
        setIsHovered(false);
    };
    return (
<div className="App">
    <div className="gradient"></div>
    <div className="section-blue">
      <section id="projects">
        <h2>My Projects</h2>
        <article>
          <div className="text">
            <h4>Latest Project - Social Media Platform</h4>
            <h3>Puppy Love</h3>
            <p className="blackbox">An Application that allows users to easily find puppy pals for their dogs. 
            I Connected our backend and frontend to AWS S3 bucket which allowed users to upload their own photos.
            I Migrated our application from the Django SQLite database to our designed Postgres database for 
            deployment. I Collaborated with four other programmers using multiple git branches, paired programming,
            and CI/CD. Our team met daily for stand-ups as we followed Agile methodology. I Implemented the user microservice and polling apparatus to share data with the main app.
            I Utilized Django rest framework to implement authentication and display profile pages specific to users</p>
            <a id="visit-github" className="btn btn-primary" href="https://gitlab.com/pawfessional-programmers/puppy-love" role="button">Visit GitLab</a>
            <h4>Technologies used include:</h4>
            <ul>
              <li>Django</li>
              <li>Docker</li>
              <li>React</li>
              <li>JavaScript</li>
              <li>Python</li>
              <li>Heroku</li>
            </ul>
          </div>
          <img 
          src={isHovered ? puppy2 : `https://puppy-love-assets.s3.us-west-1.amazonaws.com/us-west-1/dogs/Willie_logo_2.jpg`} 
          onMouseOver={handleMouseOver}
          onMouseOut={handleMouseOut}
          alt="PuppyLove project"  />
        </article>
        <article className="reverse">
          <div className="text">
            <h4>Local To-do tracker</h4>
            <h3>Dockit</h3>
            <p className="blackbox"> This app is as simple as it sounds. It is a app that you can run locally 
            to track your current list of to-dos. Check them off as you go so they are removed from your queue. 
            assign them to someone else in your family and keep track of who each task belongs to.
            </p>
            <a id="visit-github" className="btn btn-primary" href="https://gitlab.com/TaylorPanningMade/project-alpha-apr" role="button">Visit GitLab</a>
            <h4>Technologies used include:</h4>
            <ul>
              <li>Django</li>
              <li>HTML5</li>
              <li>CSS</li>
              <li>JavaScript</li>
            </ul>
          </div>
          <img src={isHovered ? todovid : `https://cdn.dribbble.com/users/932465/screenshots/5434532/media/20f8e12921fe82a02e27b8e6b30bc2dd.jpg`} 
            onMouseOver={handleMouseOver}
            onMouseOut={handleMouseOut}
            alt="Dockit App" 
            />
        </article>
        <article>
          <div className="text">
            <h4>Pair Project</h4>
            <h3>CarCar</h3>
            <p className="blackbox">
            Web Application designed to coordinate the warehouse, sales and service teams working 
            at a Car Dealership. I Conceptualized and developed app, from wireframing and relational 
            modeling to a visually appealing UI using Domain Driven Design. I implemented polling 
            built on a RESTful API using Django allowing each microservice to fully control their data. I
            implemented four microservices so each department could function independently. I also, leveraged React to create a responsive and consistent UX
 
            </p>
            <a id="visit-github" className="btn btn-primary" href="https://gitlab.com/TaylorPanningMade/project-beta-CarDealership" role="button">Visit GitLab</a>
            <h4>Technologies used include:</h4>
            <ul>
              <li>Django</li>
              <li>React</li>
              <li>Bootstrap</li>
              <li>Docker</li>
            </ul>
          </div>
            <img src={isHovered ? Carcar : carcar }
            onMouseOver={handleMouseOver}
            onMouseOut={handleMouseOut}
            alt="CarShop App" 
            />
        </article>
      <article className="reverse">
          <div className="text">
            <h4>Recipe and Grocery Management App</h4>
            <h3>Scrumptious</h3>
            <p className="blackbox"> A Recipe web application that allows the user to easily 
            track their favorite recipes while creating meal plans. In this project I 
            dynamically rendered HTML5 pages based on django authentication user information.
            </p>
            <a id="visit-github" className="btn btn-primary" href="https://gitlab.com/TaylorPanningMade/recipes-project-main" role="button">Visit GitLab</a>
            <h4>Technologies used include:</h4>
            <ul>
              <li>Django</li>
              <li>HTML5</li>
              <li>CSS</li>
              <li>JavaScript</li>
            </ul>
          </div>
          <img src={isHovered ? scrumpt1 : `https://cdn-icons-png.flaticon.com/512/1460/1460211.png`} 
            onMouseOver={handleMouseOver}
            onMouseOut={handleMouseOut}
            alt="Scrumptious App" 
            />
        </article>
    </section>
    </div>
    <div className="gradient"></div>
    <footer>
      <h2>Taylor Panning &middot; Software Engineer</h2>
      <ul>
            <li>
            <a href="http://www.linkedin.com/taylorpanning/" target="_blank">
               <span className="fa-brands fa-linkedin" aria-hidden="true"></span>
              <span className="sr-only">LinkedIn</span>
            </a>
            </li>
            <li>
            <a href="https://gitlab.com/TaylorPanningMade" target="_blank">
              <span className="fa-brands fa-square-gitlab" aria-hidden="true"></span>
              <span className="sr-only">GitLab</span>
            </a>
            </li>
            <li>
            <a href="mailto:tpanning6@gmail.com" target="_blank">
              <span className="fa-solid fa-envelopes-bulk" aria-hidden="true"></span>
              <span className="sr-only">Email Me</span>
            </a>
            </li>
      </ul>
      <p><small> &copy; 2022 Taylor Panning. All rights reserved.</small> </p>  
    </footer>
    </div>
                );
}

export default NewProjects