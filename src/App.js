import './App.css';
import React from 'react';
import Skills from "./Components/Skills";

const App = () => {
  return (
    <div className="App">
      <Skills />
    </div>
  );
};

export default App;