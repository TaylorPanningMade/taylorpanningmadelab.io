import React from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './App.css'
import DesignedResumeTaylorPanning from './assets/DesignedResumeTaylorPanning.pdf';

const Resume = () => {
    const onButtonClick = () => {
        // using Java Script method to get PDF file
        fetch('https://puppy-love-assets.s3.us-west-1.amazonaws.com/media/Taylor+Panning-Resume_PDF.pdf').then(response => {
            response.blob().then(blob => {
                // Creating new object of PDF file
                const fileURL = window.URL.createObjectURL(blob);
                // Setting various property values
                let alink = document.createElement('a');
                alink.href = fileURL;
                alink.download = 'TaylorPanningResume.pdf';
                alink.click();
            })
        })
    }

    return (

        <div className="App"
            style={{
                height: "100vh",
                width: '100vw',
                backgroundColor: "#EDF7F6",
                backgroundPosition: "center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                opacity: 1.0,
            }}
        >
            <Container fluid='lg'>
                <Row>
                    <Col>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <center>
                            <button onClick={onButtonClick}>
                                Download PDF Resume
                            </button>
                            <p> Downloaded resume contains list of projects</p>
                        </center>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                    </Col>

                </Row>
                <Row>
                    <Col className="shadow-5">
                        <center>
                            <object data={DesignedResumeTaylorPanning} type="application/pdf" width="100%" height="800" >
                                <p>Alternative text - include a link <a href="https://puppy-love-assets.s3.us-west-1.amazonaws.com/media/Taylor+Panning-Resume_PDF.pdf">to the PDF!</a></p>
                            </object>
                        </center>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                        <ul></ul>
                    </Col>
                </Row>
            </Container>


        </div >
    );
}


export default Resume

